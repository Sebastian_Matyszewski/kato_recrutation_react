import "./App.css";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import React, { Component } from 'react';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      product: [],
      checked: false,
      city: "London"
    };
  }

  appid = 'fbae81bd0f890f91ef775a957dd3ecd6';
  refresh = false;

  getProductData = async () => {
    try {
      const data = await axios.get(
        "https://api.openweathermap.org/data/2.5/forecast?q="+ this.state.city +"&appid=" + this.appid
      );
      this.setState({product: data.data.list}); 
    } catch (e) {
      console.error(e);
    }
  };

  handleChangeChecked(event) {
    this.setState({checked: event.target.checked});
    if(this.state.checked){
      clearInterval(this.interval)
    } else{
      this.interval = setInterval(() => {
        this.getProductData();
      }, 10000)
    }
  }

  onChangeCity(event){
    this.setState({city: event.target.value});
    setTimeout(() =>{
      this.getProductData();
    }); 
  }

  componentDidMount(){
    this.setState({city: "London"})
    this.getProductData();
  }

  render(){
    return (
      <div className="container d-flex flex-column align-items-center">
        <h1>Forecast {this.state.city}</h1>
        <div className="form-check m-3 d-flex flex-column align-items-center" style={this.divStyle}>
          <Form.Select
            className="m-3"
            custom="London" 
            onChange={this.onChangeCity.bind(this)}>
            <option value="London">London</option>
            <option value="München">München</option>
          </Form.Select>
        <div style={this.div2Style}>
          <input type="checkbox"
            className="form-check-input"
            onChange={this.handleChangeChecked.bind(this)}
            defaultChecked={this.state.checked}
          /> <p>refresh every 10s</p>
        </div>
        </div>
        <Table striped bordered hover variant="dark">
          <thead key="thead">
            <tr>
              <th>dt_txt</th>
              <th>humidity</th>
              <th>temp</th>
            </tr>
          </thead>
          <tbody key="tbody">
            {this.state.product.map((item, index) => {
              return (
                <tr key={'tr'+index}>
                  <td key="{item.dt_txt}">{item.dt_txt}</td>
                  <td key="{item.main.humidity}">{item.main.humidity}</td>
                  <td key="{item.main.temp}">{item.main.temp}</td>
                </tr>
              )
            })} 
          </tbody>
        </Table>
      </div>
    );
  }
};
export default App;